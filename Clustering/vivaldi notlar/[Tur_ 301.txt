[Turing Ödülü](https://tr.wikipedia.org/wiki/Turing_%C3%96d%C3%BCl%C3%BC) alan ünlü bilim adamları
* **Geoffrey Everest Hinton** :bilişsel ruhbilimci bilgisayar bilimci yapay sinir ağları konusundaki çalışmalarıyla tanındı, 2013 te google brains projesine katıldı, 2018 Turing Ödülünü Yoshua Bengio ve Yann LeCun la birlikte almaya hak kazandı
* **Yoshua Bengio**: Kanadalı bilgisayar bilimci yapay sinir ağları ve derin öğrenme konusundaki çalışmalarıyla tanınmıştır
Geoffrey Hinton ve Yann LeCun la birlikte 2018 de Turing Ödülü almıştır
Sun Sep 03 2023 19:40:29 GMT+0300 (GMT+03:00)
