İstatistik 
 NominalKategorik
 sıralıHiyerarşik Kategorik
 AralıklıSayısal Kategorik
 OrantılıSayısal
Doğrulukölçülen değerin gerçek değere yakınlığı
Hassasiyether ölçümde birbirine yakın değerlerin elde edilmesi
sınıf sayısı k  karekök n 
n  gözlem değerleri sayısı

aritmetik ortalama  medyan  mod  simetrik dağılım
aritmetik ortalama  medyan  mod  sağa çarpık dağılım
mod  medyan  aritmetik ortalama  sola çarpık dağılım

ortalama mutlak sapma gözlem değerlerinin  ortalamadan farkının alındıktan sonra mutlak değerle pozitifleştirilmesi ve gözlem değerlerinin sayısına bölünmesi böylece negatif değerlerle pozitif değerlerin birbirini sıfırlaştırmasının önüne geçilmesi
varyans gözlem değerlerinin ortalamadan farkının karesinin gözlem değerlerinin sayısının bir eksiğine bölümü
standart sapma varyansın karekökü
varyasyon katsayısı standart sapmanın aritmetik ortalamaya oranı
deneysel kural 
 gözlemlerin  i ortalamanın  sapma içinde yer alır
 gözlemlerin  i ortalamanın  sapma içerisinde yer alır
 gözlemlerin  si ortalamanın  sapma içerisinde yer alır
chebyshev teoremi k kare k  standart sapma uzaklığı
anakütle tek modlu simetrik bir dağılıma sahip değilse veya tek modlu olup sağa sola çarpık ise chebyshev teoremi uygulanır
ortalamanın standart hatası varyansın gözlem değerlerinin kareköküne oranı
moment herhangi bir sayıya göre gözlem değerlerinin herhangi bir sayıdan farkının r derecelerinin toplamının gözlem değerlerine oranı
  a göre moment alındığında birinci dereceden moment aritmetik ortalamaya
ikinci dereceden moment ise kareli ortalamanın karesine eşittir
 aritmetik ortalamaya göre moment alındığında birinci dereceden momentler sıfıra ikinci dereceden momentler ise varyansa eşittir pozitif kareköküde standart sapmaya eşittir
standart moment r dereceli momentin r dereceli varyansa oranı
çarpıklıkyatıklık 
 Pearson çarpıklık ölçüsü ortalamanın moddan farkının standart sapmaya oranı
 Pearson çarpıklık ölçüsü ortalamanın medyandan farkınınn  katının standart sapmaya oranı

üçüncü dereceden standart moment a olsun
 a   ise seri sola çarpık
 a   ise seri simetrik
 a   ise seri sağa çarpık

basıklık
 gözlem değerlerinin ortalamadan farkının  dereceden değerinin gözlem değerlerine oranının standart sapmanın  dereceden değerine oranının  ten farkı
 a   ise seri basık
 a   ise seri normal
 a   ise seri diksivri
standart değişkenpuan gözlem değerlerinin ortalamadan olan farkının standart sapmaya oranı Z değeri T değeri   Z 
Sun Dec    GMT GMT